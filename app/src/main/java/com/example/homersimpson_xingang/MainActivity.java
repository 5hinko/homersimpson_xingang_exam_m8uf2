package com.example.homersimpson_xingang;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView imgTitle;
    private ImageView imgUll;
    private ImageView imgEngVermell;
    private ImageView imgEngBlau;
    private ImageView imgEngVerd;
    private ImageView imgDonut;
    private MediaPlayer audio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgTitle = findViewById(R.id.img_title);
        imgUll = findViewById(R.id.img_ull);
        imgEngBlau = findViewById(R.id.img_engranatgeBlau);
        imgEngVerd = findViewById(R.id.img_engranatgeVerd);
        imgEngVermell = findViewById(R.id.img_engranatgeVermell);
        imgDonut = findViewById(R.id.img_donut);

        int visibleQuest = View.INVISIBLE;
        imgUll.setVisibility(visibleQuest);
        imgEngBlau.setVisibility(visibleQuest);
        imgEngVermell.setVisibility(visibleQuest);
        imgEngVerd.setVisibility(visibleQuest);
        imgDonut.setVisibility(visibleQuest);

        AnimationDrawable animationDrawable = (AnimationDrawable) imgTitle.getDrawable();
        animationDrawable.start();

        imgTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int visibleQuest;
                if (imgUll.getVisibility() == View.INVISIBLE) {
                    visibleQuest = View.VISIBLE;
                } else {
                    visibleQuest = View.INVISIBLE;
                }

                imgUll.setVisibility(visibleQuest);
                imgEngBlau.setVisibility(visibleQuest);
                imgEngVermell.setVisibility(visibleQuest);
                imgEngVerd.setVisibility(visibleQuest);
                imgDonut.setVisibility(visibleQuest);

                Animation animDre = AnimationUtils.loadAnimation(v.getContext(), R.anim.rodar_dre);
                Animation animEsq = AnimationUtils.loadAnimation(v.getContext(), R.anim.rodar_esq);
                Animation animDonut = AnimationUtils.loadAnimation(v.getContext(), R.anim.donut_anim);
                Animation animUll = AnimationUtils.loadAnimation(v.getContext(),R.anim.ull_anim);

                if(visibleQuest == View.VISIBLE){
                    imgEngVerd.startAnimation(animDre);

                    imgEngBlau.startAnimation(animEsq);
                    imgEngVermell.startAnimation(animEsq);

                    imgDonut.startAnimation(animDonut);

                    imgUll.startAnimation(animUll);
                }else{

                    imgEngVerd.clearAnimation();

                    imgEngBlau.clearAnimation();
                    imgEngVermell.clearAnimation();

                    imgDonut.clearAnimation();

                    imgUll.clearAnimation();
                }
            }
        });


        imgDonut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (audio != null && audio.isPlaying()) {
                        audio.stop();
                    } else {
                        audio = MediaPlayer.create(v.getContext().getApplicationContext(), R.raw.the_simpsons);
                        audio.start();
                    }
                }catch (Exception e){
                    Log.d("Act","No ha pogut reproducir el so");
                }
            }
        });
    }
}
